import { Injectable, HttpException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { Book } from './book.entity';

@Injectable()
export class BooksService {
    constructor(
        @InjectRepository(Book)
        private readonly bookRepository: Repository<Book>
    ) {}
    
    async getAll(): Promise<any> {
        const books = await this.bookRepository.find()
        if(!books){
            throw new HttpException('Books does not exist!', 404);
        }
        return JSON.stringify(books)
    }

    async add(book): Promise<any>{
        const results = await this.bookRepository.insert(book)
        if(!results){
            throw new HttpException('Unexpected error saving book!', 404);
        }
        return JSON.parse('{"Message": "Book successfully saved!"}')
    }

    async get(bookId: String): Promise<any> {
        let id = Number(bookId);
        const book = await this.bookRepository.findOne(id);
        if (!book) {
            throw new HttpException('Book does not exist!', 404);
        }
        return JSON.stringify(book);
    }

    async delete(bookId): Promise<any> {
        const id = Number(bookId);
        await this.bookRepository.delete(id) 
        return await this.getAll()
    }

    async update(bookId, titleArgs, descriptionArgs, authorArgs): Promise<any> {
        let id = Number(bookId);
        let book = new Book()
        book.id = id;
        book.title = titleArgs;
        book.description = descriptionArgs;
        book.author = authorArgs;
        await this.bookRepository.update(id, book)
        return await this.getAll()
    }

}
