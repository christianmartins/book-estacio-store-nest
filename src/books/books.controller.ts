import { Controller, Get, Post, Body, Param, Delete, Put, Req } from '@nestjs/common';
import { BooksService } from './books.service';
import { Request } from 'express';

@Controller('books')
export class BooksController {
    constructor(private readonly bookService: BooksService) {}

    @Get()
    async getAll(): Promise<any[]> {
        return this.bookService.getAll();
    }

    @Get(':id')
    async get(@Param('id') id: String): Promise<any[]> {
        return await this.bookService.get(id);
    }

    @Post()
    async add(@Req() request: Request): Promise<any[]>{
        return this.bookService.add(request.body)
    }

    @Delete(':id')
    async delete(@Param('id') id: String): Promise<any[]> {
        return this.bookService.delete(id);
    }

    @Put(':id')
    async update(
        @Param('id') id: String,
        @Body('title') titleArgs: String, 
        @Body('description') descriptionArgs: String, 
        @Body('author') authorArgs: String
    ): Promise<any[]> {
        return this.bookService.update(id, titleArgs, descriptionArgs, authorArgs);
    }
}
